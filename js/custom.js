// This code sample uses the 'node-fetch' library:
const boardURL =
  "https://api.trello.com/1/boards/5e576b74a8902a577caffc8f?key=cf7dca9bb22cb5290cc5e6248ae84316&token=5390e733b1f2ab2c0210ad46a701ca9b029368b6fe863fcd13ee2d2f9d7906ca";
const cardURL =
  "https://api.trello.com/1/cards/1O4DMYxd?key=cf7dca9bb22cb5290cc5e6248ae84316&token=5390e733b1f2ab2c0210ad46a701ca9b029368b6fe863fcd13ee2d2f9d7906ca";
fetch(boardURL, {
  method: "GET",
  headers: {
    Accept: "application/json",
  },
})
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.json();
  })
  .then((json) => {
    let boardName = json.name;
    const header = document.getElementById("name");
    console.log(json);
    header.innerHTML = boardName;
    console.log(boardName);
  })
  .catch((err) => console.error(err));

fetch(
  "https://api.trello.com/1/lists?key=cf7dca9bb22cb5290cc5e6248ae84316&token=5390e733b1f2ab2c0210ad46a701ca9b029368b6fe863fcd13ee2d2f9d7906ca&name=LearningPlatform&idBoard=5e576b74a8902a577caffc8f",
  {
    method: "POST",
  }
)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.json();
  })
  .then((json) => {
    let listName = json.name;
    const listheader = document.getElementById("listName");
    console.log(json);
    listheader.innerHTML = listName;
    console.log(listName);
  })
  .catch((err) => console.error(err));

fetch(
  "https://api.trello.com/1/lists?key=cf7dca9bb22cb5290cc5e6248ae84316&token=5390e733b1f2ab2c0210ad46a701ca9b029368b6fe863fcd13ee2d2f9d7906ca&name=Doing&idBoard=5e576b74a8902a577caffc8f",
  {
    method: "POST",
  }
)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.json();
  })
  .then((json) => {
    let listName2 = json.name;
    const listheader2 = document.getElementById("listName2");
    console.log(json);
    listheader2.innerHTML = listName2;
    console.log(listName2);
  })
  .catch((err) => console.error(err));

fetch(
  "https://api.trello.com/1/lists?key=cf7dca9bb22cb5290cc5e6248ae84316&token=5390e733b1f2ab2c0210ad46a701ca9b029368b6fe863fcd13ee2d2f9d7906ca&name=Done&idBoard=5e576b74a8902a577caffc8f",
  {
    method: "POST",
  }
)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.json();
  })
  .then((json) => {
    let listName3 = json.name;
    const listheader3 = document.getElementById("listName3");
    console.log(json);
    listheader3.innerHTML = listName3;
    console.log(listName3);
  })
  .catch((err) => console.error(err));

fetch(
  "https://api.trello.com/1/cards?key=cf7dca9bb22cb5290cc5e6248ae84316&token=5390e733b1f2ab2c0210ad46a701ca9b029368b6fe863fcd13ee2d2f9d7906ca&idList=60245726a66b243783b54cff&name=designDocument",
  {
    method: "POST",
    headers: {
      Accept: "application/json",
    },
  }
)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.json();
  })
  .then((json) => {
    let cardName = json.name;
    const cardheader = document.getElementById("cardName");
    console.log(json);
    cardheader.innerHTML = cardName;
    console.log(cardName);
  })
  .catch((err) => console.error(err));

fetch(
  "https://api.trello.com/1/cards?key=cf7dca9bb22cb5290cc5e6248ae84316&token=5390e733b1f2ab2c0210ad46a701ca9b029368b6fe863fcd13ee2d2f9d7906ca&idList=60245726a66b243783b54cff&name=designTemplate",
  {
    method: "POST",
  }
)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.json();
  })
  .then((json) => {
    let newCard = json.name;
    const newCardheader = document.getElementById("newCard");
    console.log(json);
    newCardheader.innerHTML = newCard;
    console.log(newCard);
  })
  .catch((err) => console.error(err));

fetch(
  "https://api.trello.com/1/cards/r5dg5ZOs?key=cf7dca9bb22cb5290cc5e6248ae84316&token=5390e733b1f2ab2c0210ad46a701ca9b029368b6fe863fcd13ee2d2f9d7906ca&name=realCard",
  {
    method: "PUT",
    headers: {
      Accept: "application/json",
    },
  }
)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.text();
  })
  .then((text) => console.log(text))
  .catch((err) => console.error(err));

fetch(
  "https://api.trello.com/1/cards/r5dg5ZOs?key=cf7dca9bb22cb5290cc5e6248ae84316&token=5390e733b1f2ab2c0210ad46a701ca9b029368b6fe863fcd13ee2d2f9d7906ca&name=realCard",
  {
    method: "PUT",
    headers: {
      Accept: "application/json",
    },
  }
)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.text();
  })
  .then((text) => console.log(text))
  .catch((err) => console.error(err));

fetch(
  "https://api.trello.com/1/cards/kwwpix6f?idList=60245f8246f84b3319e0ffcd",
  {
    method: "PUT",
    headers: {
      Accept: "application/json",
    },
  }
)
  .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.text();
  })
  .then((text) => console.log(text))
  .catch((err) => console.error(err));
